#include <stdio.h>
#include <stdlib.h>

int main()
{
	//download datasets
	system("kaggle datasets download -d bryanb/fifa-player-stats-database");

	//unzip datasets
	system("unzip fifa-player-stats-database.zip");

	//print player with potential more than 85 and not from manchester city
	system("awk -F ',' -v OFS='   ' '($3 < 25) && ($8 > 85) && ($9 != \"Manchester City\") { print $2, $3, $4, $5, $8, $9}' 'FIFA23_official_data.csv'");
	return 0;
}
