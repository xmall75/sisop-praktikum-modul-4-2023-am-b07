#define FUSE_USE_VERSION 30
#include <fuse.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <dirent.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <openssl/bio.h>
#include <openssl/evp.h>

static const char* base64_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

char* base64_encode(const unsigned char* data, size_t input_length, size_t* output_length)
{
    *output_length = 4 * ((input_length + 2) / 3);

    char* encoded_data = malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;

    int i, j;
    for (i = 0, j = 0; i < input_length;)
    {
        uint32_t oct_a = i < input_length ? data[i++] : 0;
        uint32_t oct_b = i < input_length ? data[i++] : 0;
        uint32_t oct_c = i < input_length ? data[i++] : 0;

        uint32_t triple = (oct_a << 0x10) + (oct_b << 0x08) + oct_c;

        encoded_data[j++] = base64_chars[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = base64_chars[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = base64_chars[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = base64_chars[(triple >> 0 * 6) & 0x3F];
    }

    static const char padding[] = "=";
    switch (input_length % 3)
    {
        case 1:
            encoded_data[*output_length - 1] = padding[0];
            encoded_data[*output_length - 2] = padding[0];
            break;
        case 2:
            encoded_data[*output_length - 1] = padding[0];
            break;
    }

    return encoded_data;
}

static int secretadmirer_getattr(const char* path, struct stat* stbuf)
{
    int res = 0;

    memset(stbuf, 0, sizeof(struct stat));

    // Mendapatkan atribut file/direktori asli di dalam /etc
    char real_path[PATH_MAX];
    snprintf(real_path, PATH_MAX, "/etc%s", path);

    if (lstat(real_path, stbuf) == -1)
        res = -errno;

    return res;
}

static int secretadmirer_readdir(const char* path, void* buf, fuse_fill_dir_t filler,
                                off_t offset, struct fuse_file_info* fi)
{
    DIR* dir;
    struct dirent* dirent;

    (void)offset;
    (void)fi;

    // Mendapatkan direktori asli di dalam /etc
    char real_path[PATH_MAX];
    snprintf(real_path, PATH_MAX, "/etc%s", path);

    dir = opendir(real_path);
    if (dir == NULL)
        return -errno;

    while ((dirent = readdir(dir)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));

        // Mendapatkan atribut file/direktori asli
        char real_file_path[PATH_MAX];
        snprintf(real_file_path, PATH_MAX, "%s/%s", real_path, dirent->d_name);
        if (lstat(real_file_path, &st) == -1)
            continue;

        // Enkripsi file/direktori yang diawali dengan L, U, T, atau H
        if (dirent->d_name[0] == 'L' || dirent->d_name[0] == 'U' || dirent->d_name[0] == 'T' || dirent->d_name[0] == 'H')
        {
            if (S_ISDIR(st.st_mode))
            {
                char encoded_name[NAME_MAX];
                size_t len;
                char* encoded_data = base64_encode((const unsigned char*)dirent->d_name, strlen(dirent->d_name), &len);
                snprintf(encoded_name, NAME_MAX, "%.*s", (int)len, encoded_data);
                free(encoded_data);

                // Menyimpan nama enkripsi di FUSE
                filler(buf, encoded_name, &st, 0);
            }
            else if (S_ISREG(st.st_mode))
            {
                // Mendapatkan nama file enkripsi
                char encoded_name[NAME_MAX];
                size_t len;
                char* encoded_data = base64_encode((const unsigned char*)dirent->d_name, strlen(dirent->d_name), &len);
                snprintf(encoded_name, NAME_MAX, "%.*s", (int)len, encoded_data);
                free(encoded_data);

                // Menyimpan nama enkripsi di FUSE
                filler(buf, encoded_name, &st, 0);
            }
        }
        else
        {
            // Menyimpan nama asli di FUSE
            filler(buf, dirent->d_name, &st, 0);
        }
    }

    closedir(dir);
    return 0;
}

static struct fuse_operations secretadmirer_oper = {
    .getattr = secretadmirer_getattr,
    .readdir = secretadmirer_readdir,
};

int main(int argc, char* argv[])
{
    umask(0);

    return fuse_main(argc, argv, &secretadmirer_oper, NULL);
}

