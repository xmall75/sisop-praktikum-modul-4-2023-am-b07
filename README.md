### Soal 1
Pada soal no 1, kita diminta untuk mendownload sebuah dataset dari kaggle untuk di unzip. Setelah itu kita perlu membaca file CSV khusus bernama FIFA23_official_data.csv dan mencetak data pemain yang berusia di bawah 25 tahun, memiliki potensi di atas 85, dan bermain di klub lain selain Manchester City. Semua proses ini dilakukan di dalam file storage.c. Untuk file storage.c nya adalah sebagai berikut.

```
#include <stdio.h>
#include <stdlib.h>

int main()
{
	//download datasets
	system("kaggle datasets download -d bryanb/fifa-player-stats-database");

	//unzip datasets
	system("unzip fifa-player-stats-database.zip");

	//print player with potential more than 85 and not from manchester city
	system("awk -F ',' -v OFS='   ' '($3 < 25) && ($8 > 85) && ($9 != \"Manchester City\") { print $2, $3, $4, $5, $8, $9}' 'FIFA23_official_data.csv'");
	return 0;
}
```

Pertama kita run `system("kaggle datasets download -d bryanb/fifa-player-stats-database");` yang akan mendownload dataset dari kaggle. Sebelum bisa melakukan hal ini kita perlu mendownload api kaggle dan mendapatkan tokennya terlebih dahulu. Untuk langkah-langkah tersebut dapat dibaca dari website kaggle itu sendiri.

Selanjutnya kita unzip dataset dengan `system("unzip fifa-player-stats-database.zip");`

Terakhir kita tampilkan informasi yang relevan sesuai dengan ketentuan soal menggunakan `system("awk -F ',' -v OFS='   ' '($3 < 25) && ($8 > 85) && ($9 != \"Manchester City\") { print $2, $3, $4, $5, $8, $9}' 'FIFA23_official_data.csv'");`

Hasil run : 
![1](soal1/1.png)

![2](soal1/2.png)

Setelah itu kita diminta untuk membuat sistem tersebut ke sebuah docker container. Hal ini bisa dilakukan dengan membuat Dockerfilenya terlebih dahulu.

```
FROM gcc

WORKDIR /storagefiles

COPY storage.c .
COPY FIFA23_official_data.csv .

RUN gcc -o storage storage.c

CMD ["./storage"]

```

Dockerfile tersebut akan menggunakan image gcc setelah itu mengcopy `storage.c` dan `FIFA23_official_data.csv`. Kemudian file tersebut akan mengcompile storage.c dan di run.

Hasil run Dockerfile : 

![3](soal1/3.png)

Cek imagenya menggunakan `docker image ls`:

![4](soal1/4.png)

Kita bisa mencoba run image untuk melihat apa hasilnya menggunakan `sudo docker run storage`:

![5](soal1/5.png)

Pada hal ini kaggle tidak ditemukan karena kita tidak mendownload kaggle di docker container. Saya tidak mendownload kaggle dikarenakan akan dibutuhkan kaggle token agar bisa merun fungsi kaggle tersebut. Dan dikarenakan kaggle token bisa expire saya rasa step ini kurang diperlukan dan lebih baik untuk memindahkan file .csv nya langsung.


Selanjutnya kita diminta untuk mengupload image ke dockerhub.

Pertama-tama kita bisa login dulu menggunakan `docker login`.

![6](soal1/6.png)

Setelah itu kita bisa tag image yang mau di push dan kemudian melakukan push ke repo yang kita mau :

![7](soal1/7.png)

Pada kasus ini image berhasil di push ke repo lodagos/storage-app

![8](soal1/8.png)


Pada soal terakhir kita diminta untuk melakukan docker compose di folder barcelona/napoli sebanyak 5 instance menggunakan image ytadi.

Kita bisa membuat `docker-compose.yml` nya terlebih dahulu.

```
version: '3'
services:
  storage-app:
    image: lodaogos/storage-app
    ports:
      - "8000"
    deploy:
      replicas: 5
```

Untuk `docker-compose.yml` akan menggunakan image `lodaogos/storage-app` dan mengopen ke port 8000. Untuk docker-compose di folder lain akan di ubah nanti portnya. Kemudian kita mengset `replicas: 5` agar membuat 5 instance dari image tersebut.

Hasil run adalah sebagai berikut : 

![9](soal1/9.png)

![10](soal1/10.png)

### Soal 3
Pertama, buat fungsi untuk melakukan encode ke dalam base64.
```
char* base64_encode(const unsigned char* data, size_t input_length, size_t* output_length)
{
    *output_length = 4 * ((input_length + 2) / 3);

    char* encoded_data = malloc(*output_length);
    if (encoded_data == NULL)
        return NULL;

    int i, j;
    for (i = 0, j = 0; i < input_length;)
    {
        uint32_t oct_a = i < input_length ? data[i++] : 0;
        uint32_t oct_b = i < input_length ? data[i++] : 0;
        uint32_t oct_c = i < input_length ? data[i++] : 0;

        uint32_t triple = (oct_a << 0x10) + (oct_b << 0x08) + oct_c;

        encoded_data[j++] = base64_chars[(triple >> 3 * 6) & 0x3F];
        encoded_data[j++] = base64_chars[(triple >> 2 * 6) & 0x3F];
        encoded_data[j++] = base64_chars[(triple >> 1 * 6) & 0x3F];
        encoded_data[j++] = base64_chars[(triple >> 0 * 6) & 0x3F];
    }

    static const char padding[] = "=";
    switch (input_length % 3)
    {
        case 1:
            encoded_data[*output_length - 1] = padding[0];
            encoded_data[*output_length - 2] = padding[0];
            break;
        case 2:
            encoded_data[*output_length - 1] = padding[0];
            break;
    }

    return encoded_data;
}
```

Lalu buat fungsi secretadmirer_getattr untuk mendapatkan atribut file/direktori asli di dalam direktori /etc.

```
static int secretadmirer_getattr(const char* path, struct stat* stbuf)
{
    int res = 0;

    memset(stbuf, 0, sizeof(struct stat));

    char real_path[PATH_MAX];
    snprintf(real_path, PATH_MAX, "/etc%s", path);

    if (lstat(real_path, stbuf) == -1)
        res = -errno;

    return res;
}
```
Setelah itu, kita dapat membuat fungsi secretadmirer_readdir untuk mendapatkan direktori lalu encode file/direktori yang diawali dengan L, U, T, atau H ke dalam base64. Setelah itu, simpan nama encoded ke FUSE.

```
static int secretadmirer_readdir(const char* path, void* buf, fuse_fill_dir_t filler,
                                off_t offset, struct fuse_file_info* fi)
{
    DIR* dir;
    struct dirent* dirent;

    (void)offset;
    (void)fi;

    // Mendapatkan direktori asli di dalam /etc
    char real_path[PATH_MAX];
    snprintf(real_path, PATH_MAX, "/etc%s", path);

    dir = opendir(real_path);
    if (dir == NULL)
        return -errno;

    while ((dirent = readdir(dir)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));

        // Mendapatkan atribut file/direktori asli
        char real_file_path[PATH_MAX];
        snprintf(real_file_path, PATH_MAX, "%s/%s", real_path, dirent->d_name);
        if (lstat(real_file_path, &st) == -1)
            continue;

        // Enkripsi file/direktori yang diawali dengan L, U, T, atau H
        if (dirent->d_name[0] == 'L' || dirent->d_name[0] == 'U' || dirent->d_name[0] == 'T' || dirent->d_name[0] == 'H')
        {
            if (S_ISDIR(st.st_mode))
            {
                char encoded_name[NAME_MAX];
                size_t len;
                char* encoded_data = base64_encode((const unsigned char*)dirent->d_name, strlen(dirent->d_name), &len);
                snprintf(encoded_name, NAME_MAX, "%.*s", (int)len, encoded_data);
                free(encoded_data);

                // Menyimpan nama enkripsi di FUSE
                filler(buf, encoded_name, &st, 0);
            }
            else if (S_ISREG(st.st_mode))
            {
                // Mendapatkan nama file enkripsi
                char encoded_name[NAME_MAX];
                size_t len;
                char* encoded_data = base64_encode((const unsigned char*)dirent->d_name, strlen(dirent->d_name), &len);
                snprintf(encoded_name, NAME_MAX, "%.*s", (int)len, encoded_data);
                free(encoded_data);

                // Menyimpan nama enkripsi di FUSE
                filler(buf, encoded_name, &st, 0);
            }
        }
        else
        {
            // Menyimpan nama asli di FUSE
            filler(buf, dirent->d_name, &st, 0);
        }
    }

    closedir(dir);
    return 0;
}
```

Ketika di-run, terdapat kesalahan yang sampai saat ini tidak bisa diatasi.
![Run](soal3/run.png)


### Soal 4
A. Jika Anda membuat atau mengubah nama direktori dengan awalan "module_", maka direktori tersebut akan menjadi direktori modular
```
static int xmp_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) { 
	char * enc2 = strstr(path, kode);
	if(enc2 != NULL) filter(enc2);

	char newPath[1000];
	if(strcmp(path,"/") == 0){
		path=dirPath;
		sprintf(newPath,"%s",path);
	} else sprintf(newPath, "%s%s",dirPath,path);

	int res = 0;
	struct dirent *dir;
	DIR *dp;
	(void) fi;
	(void) offset;
	dp = opendir(newPath);
	if (dp == NULL) return -errno;

	while ((dir = readdir(dp)) != NULL) {
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if(enc2 != NULL) encrypt(dir->d_name, enc2);
        	else {
			char *extension = strrchr(dir->d_name, '.');
            		if (extension != NULL && strcmp(extension, ".001") == 0) {
                		*extension = '\0';
                		decode(newPath, dir->d_name);
            		}
		}
		res = (filler(buf, dir->d_name, &st, 0));
		if(res!=0) break;
	}
	closedir(dp);
	return 0;
}
```
Ini ditandai dalam kode di dalam fungsi xmp_readdir(), di mana kondisi file diperiksa saat dibaca untuk menentukan apakah file tersebut akan dimodulasi. Jika dimodulasi, fungsi encrypt() dieksekusi, jika tidak, fungsi decode() dieksekusi.

B. Ketika Anda memodulasi sebuah direktori, modulasi juga berlaku untuk konten dari subdirektori di dalam direktori tersebut.
```
while ((dir = readdir(dp)) != NULL) {
		struct stat st;
		memset(&st, 0, sizeof(st));
		st.st_ino = dir->d_ino;
		st.st_mode = dir->d_type << 12;
		if(enc2 != NULL) encrypt(dir->d_name, enc2);
        	else {
			char *extension = strrchr(dir->d_name, '.');
            		if (extension != NULL && strcmp(extension, ".001") == 0) {
                		*extension = '\0';
                		decode(newPath, dir->d_name);
            		}
		}
		res = (filler(buf, dir->d_name, &st, 0));
		if(res!=0) break;
	}
```
Ini dicapai melalui loop while dalam kode, yang mengulang konten folder dan melakukan prosedur pengeditan yang diperlukan sesuai dengan aturan modul_.

C. File fs_module.log, yang terletak di /home/[user]/fs_module.log, dibuat untuk menyimpan daftar perintah sistem yang telah dieksekusi.
```
void logSystem(char* c, int type){
	FILE * logFile = fopen("/home/Jeje/fs_module.log", "a");
	time_t currTime;
	struct tm * time_info;
	time ( &currTime );
	time_info = localtime (&currTime);
	int yr=time_info->tm_year,mon=time_info->tm_mon,day=time_info->tm_mday,hour=time_info->tm_hour,min=time_info->tm_min,sec=time_info->tm_sec;
	if(type==1) fprintf(logFile, "REPORT::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
	else if(type==2) fprintf(logFile, "FLAG::%d%d%d-%d:%d:%d::%s\n", yr, mon, day, hour, min, sec, c);
	fclose(logFile);
}
```
Fungsi logSystem digunakan untuk mencatat informasi dengan dua parameter input: "c" (string yang akan dicatat) dan "type" (menentukan jenis log yang akan dibuat). Fungsi tersebut membuka file fs_module.log dalam mode "append" menggunakan fopen dan mendapatkan waktu saat ini menggunakan time dan localtime. Tergantung pada nilai "type" (1 untuk log "report" atau 2 untuk log "flag"), entri log ditulis ke file menggunakan fprintf dengan format yang sesuai. Akhirnya, file ditutup menggunakan fclose untuk memastikan penyimpanan log yang tepat.

D. Selama modulasi, file-file yang ada dalam direktori asli akan berubah menjadi file-file kecil dengan ukuran 1024 byte.
```
void encrypt(char* enc1, char * enc2){
    	if(strcmp(enc1, ".") == 0) return;
    	if(strcmp(enc1, "..") == 0)return; 
    	int chunks=0, i, accum;
    	char largeFileName[200];
    	sprintf(largeFileName,"%s/%s/%s",dirPath,enc2,enc1);
    	printf("%s\n",largeFileName);
    	char filename[260];
    	sprintf(filename,"%s.",largeFileName);
    	char smallFileName[300];
    	char line[1080];
    	FILE *fp1, *fp2;
    	long sizeFile = file_size(largeFileName);
    
	if (sizeFile > 1025){
		chunks = sizeFile/CHUNK + 1;
		fp1 = fopen(largeFileName, "r");
		if(fp1) {
			char number[5];
			printf("Splitting\n");
			for(i=0;i<chunks;i++) {
				accum = 0;
				sprintf(number,"%03d",i+1);
				sprintf(smallFileName, "%s%s", filename, number);
				if (segment_exists(smallFileName)) continue;
				fp2 = fopen(smallFileName, "wb");
				if (fp2) {
					while (accum < CHUNK && !feof(fp1)) {
						size_t bytesRead = fread(line, 1, sizeof(line), fp1);
						size_t bytesToWrite = bytesRead;
						if (accum + bytesToWrite > CHUNK) {
							bytesToWrite = CHUNK - accum;
						}
						fwrite(line, 1, bytesToWrite, fp2);
						accum += bytesToWrite;
					}
					fclose(fp2);
				}
			}
			fclose(fp1);
			if (remove(largeFileName) != 0) printf("Gagal menghapus file asli: %s\n", largeFileName);
			else printf("File asli dihapus: %s\n", largeFileName);
		}
	}
    	printf("keluar\n");
}
```
Fungsi dimulai dengan menghitung jumlah file kecil yang akan dihasilkan dengan membagi file besar. Kemudian, membentuk jalur lengkap dari file besar dan membuat nama dasar untuk file-file kecil. Dalam loop, data dibaca dari file besar dan ditulis ke file-file kecil hingga mencapai batas ukuran atau akhir file besar. File-file kecil ditutup, dan file besar dihapus. Jika penghapusan gagal, pesan kesalahan ditampilkan.

E. Jika sebuah direktori modular diubah namanya menjadi non-modular, konten direktori tersebut akan dikembalikan ke keadaan semula.
```
void decode(const char *directoryPath, const char *prefix) {
    	DIR *dir;
    	struct dirent *entry;
    	char filePath[1000];
    	char mergedFilePath[1000];
   	FILE *mergedFile;
    	size_t prefixLength = strlen(prefix);
    	dir = opendir(directoryPath);
    	sprintf(mergedFilePath, "%s/%s", directoryPath, prefix);
    	mergedFile = fopen(mergedFilePath, "w");

    	while ((entry = readdir(dir)) != NULL) {
        	if (entry->d_type == DT_REG && strncmp(entry->d_name, prefix, prefixLength) == 0) {
            		sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
            		FILE *file = fopen(filePath, "r");
            		if (strcmp(entry->d_name, prefix) == 0) {
                		fclose(file);
                		continue;
            		}
            		char buffer[1024];
            		size_t bytesRead;
            		while ((bytesRead = fread(buffer, 1, sizeof(buffer), file)) > 0) {
                		fwrite(buffer, 1, bytesRead, mergedFile);
            		}
            		fclose(file);
        	}	 

    	}
    	closedir(dir);
    	fclose(mergedFile);
    	dir = opendir(directoryPath);

    	while ((entry = readdir(dir)) != NULL) {
        	if (entry->d_type == DT_REG) {
            		if (strncmp(entry->d_name, prefix, prefixLength) == 0) {
                		char extension[1000];
                		sprintf(extension, ".%03d", atoi(entry->d_name + prefixLength + 1));
                		if (strstr(entry->d_name, extension) != NULL) {
                    			sprintf(filePath, "%s/%s", directoryPath, entry->d_name);
                    			if (strcmp(filePath, mergedFilePath) != 0) {
                        			remove(filePath);
                    			}
                		}
            		}
        	}
    	}
    	closedir(dir);
}
```
Fungsi ini bertanggung jawab untuk menggabungkan file-file kecil menjadi satu file besar dalam sebuah direktori. Fungsi tersebut membuka direktori, membentuk jalur lengkap untuk file besar yang akan digabungkan, dan membukanya dalam mode "write". Kemudian, dilakukan loop untuk membaca setiap entri dalam direktori, mengidentifikasi file-file kecil yang akan digabungkan, membaca data dari setiap file kecil, dan menuliskannya ke file besar. Setelah loop selesai, file-file kecil, direktori, dan file besar ditutup. Kemudian, direktori dibuka kembali untuk memproses entri yang tersisa, dan loop lain dilakukan untuk menangani setiap entri, menghapus file-file kecil yang telah digabungkan kecuali file besar itu sendiri. Akhirnya, direktori ditutup kembali.
